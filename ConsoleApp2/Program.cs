﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        public static string APP_PATH = new FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).DirectoryName;

        static void Main(string[] args)
        {
            Console.WriteLine("Press any key to shutdown");

            var dotnet = new VlcDotNetTest();
            var vlcsharp = new LibVLCSharpTest();

            dotnet.StartStream("4000");
            vlcsharp.StartStream("4001");

            Console.ReadKey();

            dotnet.Stop();
            vlcsharp.Stop();
        }
    }
}
