﻿using LibVLCSharp.Shared;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Vlc.DotNet.Core;
using Vlc.DotNet.Core.Interops.Signatures;

namespace ConsoleApp2
{
    class VlcDotNetTest
    {
        private static DirectoryInfo VLCPATH = new DirectoryInfo(
            Path.Combine(
                Program.APP_PATH,
                "libvlc",
                IntPtr.Size == 4 ? "win-x86" : "win-x64"
                ));

        private static string[] VLCOPTIONS = new string[]
        {
            "--avcodec-hw=none"
        };

        private static string SOURCE = "rtsp://184.72.239.149/vod/mp4:BigBuckBunny_115k.mov";
        private static string DESTINATION = @":sout=#transcode{vcodec=theo,vb=2000,acodec=vorb,ab=128,channels=2,samplerate=44100,scodec=none}:duplicate{dst=http{mux=ogg,dst=:$PORT$/stream},dst=file{dst=$FILE$}}";

        private VlcMediaPlayer _vlc;

        private string[] _mediaOptions = new string[]
        {
            ":network-caching=1000",
            String.Empty,
            ":no-sout-all",
            ":sout-keep"
        };

        public int VehicleId { get; private set; }

        public bool IsPlaying
        {
            get { return _vlc.IsPlaying(); }
        }

        public VlcDotNetTest()
        {
            _vlc = new VlcMediaPlayer(VLCPATH, VLCOPTIONS);
            // uncomment if needed
            //_vlc.Log += OnLog;
        }

        public void StartStream(string port)
        {
            string filePath = Path.Combine(
                Program.APP_PATH,
                "testVlcDotnet.ogg"
            );

            _mediaOptions[1] = DESTINATION.Replace("$PORT$", port).Replace("$FILE$", filePath);
            _vlc.SetMedia(SOURCE, _mediaOptions);
            _vlc.Play();
        }

        public void Stop()
        {
            _vlc.Stop();
        }

        private void OnLog(object sender, VlcMediaPlayerLogEventArgs e)
        {
            Console.WriteLine("{0:HH::mm::ss}|{1, 10}|{2}", DateTime.Now, e.Level, e.Message);
        }
    }


    class LibVLCSharpTest
    {
        private static string[] VLCOPTIONS = new string[]
        {
            "--avcodec-hw=none"
        };

        private static string SOURCE = "rtsp://184.72.239.149/vod/mp4:BigBuckBunny_115k.mov";
        private static string DESTINATION = @":sout=#transcode{vcodec=theo,vb=2000,acodec=vorb,ab=128,channels=2,samplerate=44100,scodec=none}:duplicate{dst=http{mux=ogg,dst=:$PORT$/stream},dst=file{dst=$FILE$}}";

        private LibVLC _vlc;
        private MediaPlayer _mediaPlayer;

        private string[] _mediaOptions = new string[]
        {
            ":network-caching=1000",
            String.Empty,
            ":no-sout-all",
            ":sout-keep"
        };

        public LibVLCSharpTest()
        {
            Core.Initialize();

            _vlc = new LibVLC(VLCOPTIONS);
            // somehow this is not appreciated...
            //_vlc.Log += OnLog;

            _mediaPlayer = new MediaPlayer(_vlc);
        }

        public void StartStream(string port)
        {
            string filePath = Path.Combine(
                Program.APP_PATH,
                "testVlcSharp.ogg"
            );

            _mediaOptions[1] = DESTINATION.Replace("$PORT$", port).Replace("$FILE$", filePath);

            Media media = new Media(_vlc, SOURCE, Media.FromType.FromLocation);
            foreach (string opt in _mediaOptions)
                media.AddOption(opt);

            _mediaPlayer.Play(media);
        }

        public void Stop()
        {
            _mediaPlayer.Stop();
        }

        private void OnLog(object sender, LogEventArgs e)
        {
            Console.WriteLine("{0:HH::mm::ss}|{1, 10}|{2}", DateTime.Now, e.Level, e.Message);
        }
    }
}
 