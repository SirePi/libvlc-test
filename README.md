## libvlc-test

For some reasons it seems that VS can't automatically copy libvlc in the target folder when restored from scratch.
VideoLAN.LibVLC.Windows' nuGet package will have to be added manually to the solution before compiling the first time.

After the application starts, open test.html in a browser to see the result of both Vlc.DotNet and LibVLCSharp